package ar.com.copypaste.database;

import ar.com.copypaste.database.repository.ClientAmountRepository;
import ar.com.copypaste.database.repository.RepositoryFactory;
import com.typesafe.config.Config;
import org.apache.commons.dbcp2.BasicDataSource;

public class DatabaseManager {

	private BasicDataSource datasource;
	private static DatabaseManager instance;

	private static final String ENGINE = "mysql";
	private static final String DRIVER = "jdbc";

	private DatabaseManager(final Config config) {

		final String host 	= config.getString("host");
		final Integer port 	= config.getInt("port");
		final String dbName = config.getString("db");

		datasource = new BasicDataSource();
		datasource.setDriverClassName(config.getString("driver"));

		datasource.setUrl(String.format("%1$s:%2$s://%3$s:%4$s/%5$s", DRIVER, ENGINE, host, port, dbName));
		datasource.setUsername(config.getString("user"));
		datasource.setPassword(config.getString("password"));
		datasource.setInitialSize(config.getInt("pool-initial-size"));
		datasource.setMaxTotal(config.getInt("pool-total-size"));

		RepositoryFactory.init(datasource);
	}

	public static DatabaseManager fromConfig(final Config config) {
		if(instance==null) {
			instance = new DatabaseManager(config);
		}
		return instance;
	}

	public static DatabaseManager getInstance() {
		return instance;
	}

	public BasicDataSource getDatasource() {
		return datasource;
	}
}