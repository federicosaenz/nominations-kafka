package ar.com.copypaste.database.entity;

public class Cpm implements Entity{

	private Integer clientId;

	private Integer cpm;

	public Cpm() {
	}

	public Integer getClientId() {
		return clientId;
	}

	public Cpm setClientId(Integer clientId) {
		this.clientId = clientId;
		return this;
	}

	public Integer getCpm() {
		return cpm;
	}

	public Cpm setCpm(Integer cpm) {
		this.cpm = cpm;
		return this;
	}
}