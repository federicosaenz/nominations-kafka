package ar.com.copypaste.database.entity;


public class ClientAmount implements Entity {

	private Integer clientId;

	private Long amount;

	public ClientAmount() {
	}

	public ClientAmount(Integer clientId, Long amount) {
		this.clientId = clientId;
		this.amount = amount;
	}

	public Integer getClientId() {
		return clientId;
	}

	public ClientAmount setClientId(Integer clientId) {
		this.clientId = clientId;
		return this;
	}

	public Long getAmount() {
		return amount;
	}

	public ClientAmount setAmount(Long amount) {
		this.amount = amount;
		return this;
	}
}