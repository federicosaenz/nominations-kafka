package ar.com.copypaste.database.repository;

import java.sql.ResultSet;

public interface ResultSetMapper<T> {
	T map(ResultSet resultSet) throws java.sql.SQLException;
}
