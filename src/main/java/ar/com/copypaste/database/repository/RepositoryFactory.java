package ar.com.copypaste.database.repository;

import ar.com.copypaste.database.exception.InvalidRepositoryException;
import org.apache.commons.dbcp2.BasicDataSource;
import java.lang.reflect.Constructor;

public class RepositoryFactory {

	private static BasicDataSource datasource;

	public static void init(BasicDataSource ds) {
		datasource = ds;
	}

	public static <R extends Repository> R createForClass(Class<R> repository) throws InvalidRepositoryException {
		try {
			Constructor constructor = repository.getConstructor(BasicDataSource.class);
			return (R)constructor.newInstance(datasource);
		} catch (Exception e) {
			throw new InvalidRepositoryException();
		}
	}
}