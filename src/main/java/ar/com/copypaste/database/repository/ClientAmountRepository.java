package ar.com.copypaste.database.repository;

import ar.com.copypaste.database.entity.ClientAmount;
import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.ScalarHandler;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ClientAmountRepository implements Repository<ClientAmount> {

	private static final String TABLE_NAME = "clientamount";

	private final BasicDataSource datasource;

	public ClientAmountRepository(BasicDataSource datasource) {
		this.datasource = datasource;
	}

	enum Column {
		CLIENT_ID("client_id"),
		AMOUNT("amount");

		private String columnName;

		Column(String columnName) {
			this.columnName = columnName;
		}

		@Override
		public String toString() {
			return columnName;
		}
	}


	public String getTableName() {
		return TABLE_NAME;
	}


	@Override
	public void insert(ClientAmount amount) throws SQLException {

		QueryRunnerAdapter runner = new QueryRunnerAdapter(datasource);

		String insertQuery = String.format(
			"INSERT INTO %1$s (%2$s,%3$s) VALUES (?,?)",
			TABLE_NAME,
			Column.CLIENT_ID,
			Column.AMOUNT
		);

		runner.insert(
			insertQuery,
			new ScalarHandler<>(),
			amount.getClientId(),
			amount.getAmount()
		);
	}

	public ClientAmount update(ClientAmount clientAmount) throws SQLException {
		QueryRunnerAdapter runner = new QueryRunnerAdapter(datasource);

		MapperResultSetHandler<ClientAmount> resultSetHandler = new MapperResultSetHandler<>(new Resultset());

		String updateQuery = String.format(
				"UPDATE `%1$s` SET %2$s=? WHERE %3$s=?",
				TABLE_NAME,
				Column.AMOUNT,
				Column.CLIENT_ID
		);

		runner.update(
			updateQuery,
			resultSetHandler,
			clientAmount.getAmount(),
			clientAmount.getClientId()
		);

		return clientAmount;
	}

	public ClientAmount getById(Integer clientId) throws SQLException {
		QueryRunnerAdapter runner = new QueryRunnerAdapter(datasource);

		MapperResultSetHandler<ClientAmount> resultSetHandler = new MapperResultSetHandler<>(new Resultset());

		String selectQuery = String.format(
				"SELECT `%1$s`,`%2$s` FROM %3$s WHERE %4$s=?",
				Column.CLIENT_ID,
				Column.AMOUNT,
				TABLE_NAME,
				Column.CLIENT_ID
		);

		return runner.query(selectQuery,resultSetHandler,clientId);
	}

	public void upsert(ClientAmount clientAmount) throws SQLException {

		ClientAmount amount = getById(clientAmount.getClientId());
		if(amount==null) {
			insert(clientAmount);
		} else {
			update(clientAmount);
		}
	}


	class Resultset implements ResultSetMapper<ClientAmount> {

		@Override
		public ClientAmount map(ResultSet resultSet) throws SQLException {
			return new ClientAmount(
					resultSet.getInt(Column.CLIENT_ID.columnName),
					resultSet.getLong(Column.AMOUNT.columnName)
			);
		}
	}
}
