package ar.com.copypaste.database.repository;

import ar.com.copypaste.database.entity.Entity;

import java.sql.SQLException;

public interface Repository<E extends Entity> {

	void insert(E amount) throws SQLException;

	String getTableName();
}
