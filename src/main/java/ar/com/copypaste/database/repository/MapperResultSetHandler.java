package ar.com.copypaste.database.repository;

import org.apache.commons.dbutils.ResultSetHandler;

import java.sql.ResultSet;

public class MapperResultSetHandler<T> implements ResultSetHandler<T> {

	private final ResultSetMapper<T> mapper;

	public MapperResultSetHandler(final ResultSetMapper<T> mapper) {
		this.mapper = mapper;
	}

	@Override
	public T handle(final ResultSet resultSet) throws java.sql.SQLException {
		if (!resultSet.next()) {
			return null;
		}
		return mapper.map(resultSet);
	}
}
