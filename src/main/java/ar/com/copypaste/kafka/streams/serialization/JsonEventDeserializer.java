package ar.com.copypaste.kafka.streams.serialization;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.common.errors.SerializationException;
import org.apache.kafka.common.serialization.Deserializer;

import java.util.Map;

public class JsonEventDeserializer<T> implements Deserializer<T> {

	private ObjectMapper objectMapper = new ObjectMapper();

	private Class<T> tClass;

	/**
	 * Default constructor needed by Kafka
	 */
	public JsonEventDeserializer() {
	}

	@SuppressWarnings("unchecked")
	@Override
	public void configure(Map<String, ?> props, boolean isKey) {
		tClass = (Class<T>) props.get("JsonPOJOClass");
	}

	@Override
	public T deserialize(String topic, byte[] data) {
		if(data==null) {
			return null;
		}

		T event;
		try {
			event = objectMapper.readValue(data,tClass);
		} catch (Exception e) {
			System.out.println(e);
			throw new SerializationException(e);
		}

		return event;
	}

	@Override
	public void close() {

	}
}
