package ar.com.copypaste.kafka.streams;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigList;

import java.util.ArrayList;
import java.util.List;

public class StreamConfiguration {

	private static StreamConfiguration instance;

	private final List<String> servers;
	private boolean log;
	private final String streamName;

	public StreamConfiguration(
			final List<String> servers,
			final String streamName,
			boolean log
	) {
		this.servers = servers;
		this.log = log;
		this.streamName = streamName;
	}

	public StreamConfiguration(
			final String streamName,
			boolean log
	) {
		this(new ArrayList<>(),streamName,log);
	}

	public StreamConfiguration(
			final List<String> servers,
			final String streamName
	) {
		this(servers,streamName, false);
	}

	public StreamConfiguration(
			final String streamName
	) {
		this(streamName,false);
	}

	public static StreamConfiguration fromConfig(final Config config) {
		if(instance==null) {
			ConfigList servers = config.getList("servers");
			String streamName = config.getString("name");

			instance = new StreamConfiguration(streamName);
			servers.forEach(srv -> instance.addServer(srv.unwrapped().toString()));
		}
		return instance;
	}

	public StreamConfiguration addServer(String server) {
		this.servers.add(server);
		return this;
	}

	public String getServers() {
		return String.join(",", servers);
	}

	public String getStreamName() {
		return streamName;
	}

}