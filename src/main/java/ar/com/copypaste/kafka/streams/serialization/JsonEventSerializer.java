package ar.com.copypaste.kafka.streams.serialization;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.common.errors.SerializationException;
import org.apache.kafka.common.serialization.Serializer;

import java.util.Map;

public class JsonEventSerializer<T> implements Serializer<T> {

	private final ObjectMapper objectMapper = new ObjectMapper();

	/**
	 * Default constructor needed by Kafka
	 */
	public JsonEventSerializer() {
	}

	@Override
	public void configure(Map<String, ?> props, boolean isKey) {
	}

	@Override
	public byte[] serialize(String topic, T event) {
		if(event==null) {
			return null;
		}

		try {
			return objectMapper.writeValueAsBytes(event);
		} catch (Exception e) {
			throw new SerializationException("Error trying to serialize event");
		}
	}
}