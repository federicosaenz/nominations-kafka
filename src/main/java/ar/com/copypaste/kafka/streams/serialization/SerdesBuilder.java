package ar.com.copypaste.kafka.streams.serialization;

import ar.com.copypaste.kafka.entity.ClientPrice;
import ar.com.copypaste.kafka.entity.IncomingEvent;
import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.serialization.Serializer;

import java.util.HashMap;
import java.util.Map;

public class SerdesBuilder {

	private static Map<String, Object> serdeProps = new HashMap<>();

	public static final Serde<IncomingEvent> incomingEvent() {
		final Serializer<IncomingEvent> serializer = new JsonEventSerializer<>();
		serdeProps.put("JsonPOJOClass",IncomingEvent.class);
		serializer.configure(serdeProps,false);

		final Deserializer<IncomingEvent> deserializer = new JsonEventDeserializer<>();
		serdeProps.put("JsonPOJOClass",IncomingEvent.class);
		deserializer.configure(serdeProps,false);

		return Serdes.serdeFrom(serializer, deserializer);
	}

	public static final Serde<ClientPrice> clientPrice() {
		final Serializer<ClientPrice> serializer = new JsonEventSerializer<>();
		serdeProps.put("JsonPOJOClass", ClientPrice.class);
		serializer.configure(serdeProps,false);

		final Deserializer<ClientPrice> deserializer = new JsonEventDeserializer<>();
		serdeProps.put("JsonPOJOClass",ClientPrice.class);
		deserializer.configure(serdeProps,false);

		return Serdes.serdeFrom(serializer, deserializer);
	}
}
