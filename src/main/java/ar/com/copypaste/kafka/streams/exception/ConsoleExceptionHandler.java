package ar.com.copypaste.kafka.streams.exception;


import org.apache.kafka.streams.errors.StreamsUncaughtExceptionHandler;

import java.util.logging.Level;
import java.util.logging.Logger;

public class ConsoleExceptionHandler implements StreamsUncaughtExceptionHandler {

	private static final Logger logger = Logger.getLogger(ConsoleExceptionHandler.class.getName());

	@Override
	public StreamThreadExceptionResponse handle(Throwable throwable) {
		logger.log(Level.SEVERE, "a", throwable);
		return StreamThreadExceptionResponse.SHUTDOWN_APPLICATION;
	}

//
//
//	@Override
//	public void uncaughtException(Thread thread, Throwable throwable) {
//
//	}



//	public static final Thread.UncaughtExceptionHandler getConsoleExceptionHandler() {
//		return (thread, throwable) -> logger.log(Level.SEVERE, "a", throwable);
//	}
}
