package ar.com.copypaste.kafka.streams;

import ar.com.copypaste.kafka.entity.IncomingEvent;
import ar.com.copypaste.kafka.streams.serialization.SerdesBuilder;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.kstream.*;

public class IncomingEventsStream {

	private final StreamsBuilder builder;
	private final String topic;

	private final static String KTABLE_GROUPED_NAME = "countIncomingEvents";

	public IncomingEventsStream(StreamsBuilder builder, String topic) {
		this.builder = builder;
		this.topic = topic;
	}

	public final KStream<Integer, IncomingEvent> applyEventTypeFilter(String eventType) {
		return builder
				.stream(topic, Consumed.with(Serdes.Integer(), SerdesBuilder.incomingEvent()))
				.filter((key,incomingEvent)->incomingEvent.getEventType().equals(eventType));
	}

	public final KTable<Integer, Long> groupAndCountByClientId(final KStream<Integer,IncomingEvent> stream) {
		return stream
			.groupBy(
				(key,incomingEvent) ->incomingEvent.getClientID(),
				Grouped.with(Serdes.Integer(), SerdesBuilder.incomingEvent())
			)
			.count(Materialized.as(KTABLE_GROUPED_NAME));
	}
}