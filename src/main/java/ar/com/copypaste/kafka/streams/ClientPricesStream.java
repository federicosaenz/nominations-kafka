package ar.com.copypaste.kafka.streams;

import ar.com.copypaste.kafka.entity.ClientPrice;
import ar.com.copypaste.kafka.streams.serialization.SerdesBuilder;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.kstream.*;

public class ClientPricesStream {

	private final StreamsBuilder builder;

	private final String topic;

	private final KStream<Integer,ClientPrice> stream;

	public ClientPricesStream(StreamsBuilder builder, String topic) {
		this.builder = builder;
		this.topic = topic;
		stream = builder.stream(topic, Consumed.with(Serdes.Integer(), SerdesBuilder.clientPrice()));
	}


	public final KStream<Integer, ClientPrice> getStream() {
		return stream;
	}
}