package ar.com.copypaste.kafka;

import ar.com.copypaste.kafka.streams.ClientPricesStream;
import ar.com.copypaste.kafka.streams.IncomingEventsStream;
import ar.com.copypaste.kafka.streams.StreamConfiguration;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.errors.StreamsUncaughtExceptionHandler;

import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

public class KafkaManager {

	private final Properties props = new Properties();
	private final StreamsBuilder builder = new StreamsBuilder();
	private static KafkaManager instance;
	private KafkaStreams streams;
	private KafkaProducer<Integer,Integer> goal;
	private StreamsUncaughtExceptionHandler exceptionHandler;

	private final Logger logger = Logger.getLogger(KafkaManager.class.getName());

	private KafkaManager(StreamConfiguration configuration) {
		this
			.config(configuration)
			.createGoalProducer();
	}

	public static KafkaManager build(final StreamConfiguration configuration) {
		if(instance==null) {
			instance = new KafkaManager(configuration);
		}
		return instance;
	}

	public KafkaManager withExceptionHandler(StreamsUncaughtExceptionHandler handler) {
		exceptionHandler = handler;
		return this;
	}

	public static KafkaManager getInstance() {
		return instance;
	}

	private final KafkaManager config(StreamConfiguration configuration) {
		props.put(StreamsConfig.APPLICATION_ID_CONFIG, configuration.getStreamName());
		props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, configuration.getServers());
		props.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass());
		props.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass());

		return this;
	}

	public final ClientPricesStream buildClientPricesStream() {
		return new ClientPricesStream(builder,"client_prices");
	}

	public final IncomingEventsStream buildIncomingEventsStream() {
		return new IncomingEventsStream(builder, "incoming_events");
	}

	final KafkaManager createGoalProducer() {
		props.put(ProducerConfig.CLIENT_ID_CONFIG, "goal");
		props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, Serdes.Integer().serializer().getClass());
		props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, Serdes.Integer().serializer().getClass());

		goal = new KafkaProducer<>(props);
		return this;
	}

	public final void produceGoal(Integer clientId) {
		final ProducerRecord<Integer,Integer> clientPriceRecord = new ProducerRecord("goal",clientId);
		goal.send(clientPriceRecord,(recordMetadata,e)-> logger.log(Level.INFO,"Client id="+clientId+" reached the goal"));
	}

	public final KafkaManager start() {

		streams = new KafkaStreams(builder.build(), props);
		if(exceptionHandler!=null) {
			streams.setUncaughtExceptionHandler(exceptionHandler);

		}
		streams.start();

		return this;
	}



}

