package ar.com.copypaste.kafka.entity;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ClientPrice implements KafkaEntity {

	@JsonProperty
	private Integer clientID;
	@JsonProperty
	private Integer cpm;

	public ClientPrice() {
	}

	public ClientPrice(Integer clientID, Integer cpm) {
		this.clientID = clientID;
		this.cpm = cpm;
	}

	public Integer getClientID() {
		return clientID;
	}

	public Integer getCpm() {
		return cpm;
	}
}