package ar.com.copypaste.kafka.entity;

import com.fasterxml.jackson.annotation.JsonProperty;

public class IncomingEvent implements KafkaEntity {

	@JsonProperty
	private Integer clientID;
	@JsonProperty
	private String eventType;

	// Only for Jackson
	public IncomingEvent() {
	}

	public IncomingEvent(Integer clientID, String eventType) {
		this.clientID = clientID;
		this.eventType = eventType;
	}

	public Integer getClientID() {
		return clientID;
	}

	public String getEventType() {
		return eventType;
	}
}
