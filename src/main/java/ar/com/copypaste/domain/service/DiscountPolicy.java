package ar.com.copypaste.domain.service;

public class DiscountPolicy {

	private final Long impressions;
	private final Integer cpm;
	private Integer amount = 0;

	public DiscountPolicy(Long impressions, Integer cpm) {
		this.impressions = impressions;
		this.cpm = cpm;
		apply();
	}

	private DiscountPolicy applyDiscountFor1000Impressions() {
		amount += (impressions<1000) ? cpm/2 : 0;
		return this;
	}

	private DiscountPolicy applyDiscountFor5000Impressions() {
		amount += (impressions<5000) ? (impressions.intValue()/1000*cpm)*3/4 : 0;
		return this;
	}

	private DiscountPolicy applyDiscountForMoreTo5000Impressions() {
		amount += (impressions>5000) ? (impressions.intValue()/1000*cpm) : 0;
		return this;
	}

	public Integer getAmount() {
		return amount;
	}

	private Integer apply() {
		return this
			.applyDiscountFor1000Impressions()
			.applyDiscountFor5000Impressions()
			.applyDiscountForMoreTo5000Impressions()
			.getAmount();
	}
}