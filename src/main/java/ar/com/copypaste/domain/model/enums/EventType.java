package ar.com.copypaste.domain.model.enums;

import java.util.Arrays;
import java.util.Optional;

public enum EventType {

	IMP("imp"),
	VIEW("view"),
	CLICK("click"),
	LEAD("lead");

	private final String name;

	EventType(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public static Optional<EventType> getByName(String name) {
		return Arrays
			.stream(values())
			.filter(eventType->eventType.getName().equals(name.toLowerCase()))
			.findFirst();
	}
}
