package ar.com.copypaste;

import com.typesafe.config.ConfigFactory;
public class NominationsKafka {

	public static void main(final String[] args) {

		Application
			.build(ConfigFactory.load("development.conf"))
			.run();
	}
}