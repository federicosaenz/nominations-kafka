package ar.com.copypaste;

import ar.com.copypaste.database.DatabaseManager;
import ar.com.copypaste.database.entity.ClientAmount;
import ar.com.copypaste.database.repository.ClientAmountRepository;
import ar.com.copypaste.database.repository.RepositoryFactory;
import ar.com.copypaste.domain.model.enums.EventType;
import ar.com.copypaste.domain.service.DiscountPolicy;
import ar.com.copypaste.kafka.streams.ClientPricesStream;
import ar.com.copypaste.kafka.streams.IncomingEventsStream;
import ar.com.copypaste.kafka.KafkaManager;
import ar.com.copypaste.kafka.streams.StreamConfiguration;
import ar.com.copypaste.kafka.streams.exception.ConsoleExceptionHandler;
import ar.com.copypaste.redis.RedisManager;
import ar.com.copypaste.redis.entity.ClientPrice;
import com.typesafe.config.Config;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.streams.kstream.KStream;

import java.util.logging.Level;
import java.util.logging.Logger;

public class Application {

	private static final Integer GOAL_AMOUNT = 100;

	private static Application instance;

	private final Config config;

	private KStream<Integer,Long> amounts;
	private KafkaProducer<Integer,Integer> goal;

	private Logger logger = Logger.getLogger(Application.class.getName());

	private Application(Config config) {
		this.config = config;
		DatabaseManager.fromConfig(config.getConfig("database"));

		KafkaManager
			.build(StreamConfiguration.fromConfig(config.getConfig("kafka")))
			.withExceptionHandler(new ConsoleExceptionHandler());
	}

	static Application build(Config config) {
		if(instance==null) {
			instance = new Application(config);
		}
		return instance;
	}

	public static Application getInstance() {
		return instance;
	}

	private Integer getCpmByClientId(Integer clientId) {
		// From Redis or mysql
		return RedisManager.getInstance().getClientCpmById(clientId);
	}

	private Application createClientPricesStream() {
		ClientPricesStream clientPricesStream = KafkaManager.getInstance().buildClientPricesStream();

		clientPricesStream.getStream().foreach((index, clientPrice)->
			RedisManager.getInstance().insertClientPrice(new ClientPrice(clientPrice.getClientID(),clientPrice.getCpm()))
		);
		return this;
	}

	private Application createAmountsStream() {
		if(amounts==null) {
			IncomingEventsStream incomingEventsStream = KafkaManager.getInstance().buildIncomingEventsStream();

			amounts = incomingEventsStream
				.groupAndCountByClientId(incomingEventsStream.applyEventTypeFilter(EventType.IMP.getName()))
				.toStream(
					(clientId, impressionsCount) ->
						new DiscountPolicy(impressionsCount, getCpmByClientId(clientId)).getAmount()
				);
		}

		return this;
	}

	private Application processAmounts() {
		amounts.foreach((clientId,amount)-> {

			// Produce to goals
			if(amount>GOAL_AMOUNT) {
				KafkaManager.getInstance().produceGoal(clientId);
			}

			//Insert to database
			try {
				RepositoryFactory
					.createForClass(ClientAmountRepository.class)
					.upsert(new ClientAmount(clientId, amount));
			} catch (Exception e) {
				logger.log(Level.SEVERE,"Can't insert amount of "+ amount +" for client '" + clientId + "'");
			}
		});

		return this;
	}

	private Application startStreams() {
		try {
			KafkaManager.getInstance().start();
		} catch (Exception e) {
			logger.log(Level.SEVERE,"Something went wrong");
		}
		return this;
	}

	void run() {
		this
			.createClientPricesStream()
			.createAmountsStream()
			.processAmounts()
			.startStreams();
	}
}