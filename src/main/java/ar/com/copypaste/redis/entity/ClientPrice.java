package ar.com.copypaste.redis.entity;

import ar.com.copypaste.kafka.entity.KafkaEntity;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ClientPrice {

	private Integer clientID;
	private Integer cpm;

	public ClientPrice() {
	}

	public ClientPrice(Integer clientID, Integer cpm) {
		this.clientID = clientID;
		this.cpm = cpm;
	}

	public Integer getClientID() {
		return clientID;
	}

	public Integer getCpm() {
		return cpm;
	}
}