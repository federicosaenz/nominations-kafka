package ar.com.copypaste.redis;

import ar.com.copypaste.redis.entity.ClientPrice;
import com.lambdaworks.redis.RedisClient;
import com.lambdaworks.redis.RedisURI;
import com.lambdaworks.redis.api.StatefulRedisConnection;
import com.typesafe.config.Config;

public class RedisManager {

	private static RedisManager instance;

	private StatefulRedisConnection<String,String> connection;

	private String CLIENT_CPM_KEY_FORMAT = "CL:CPM";

	private RedisManager(Config config) {
		final String host 	= config.getString("host");
		final Integer port 	= config.getInt("port");
		final String password = config.getString("password");

		RedisClient redis = RedisClient.create();
		connection = redis.connect(RedisURI.create(
			String.format("redis://%1$s@%2$s:%3$s", password, host,port))
		);
	}

	public static RedisManager fromConfig(Config config) {
		if(instance==null) {
			instance = new RedisManager(config);
		}
		return instance;
	}

	public static RedisManager getInstance() {
		if(instance==null) {
			throw new RuntimeException("RedisManager not initialized. Call fromConfig() method first");
		}
		return instance;
	}

	// TODO: Move this to ClientPrice repository
	public RedisManager insertClientPrice(ClientPrice clientPrice) {
		connection.sync().hset(
			CLIENT_CPM_KEY_FORMAT,
			clientPrice.getClientID().toString(),
			clientPrice.getCpm().toString()
		);

		return this;
	}

	// TODO: Move this to ClientPrice repository
	public Integer getClientCpmById(Integer clientId) {
		String cpmString = connection.sync().hget(
				CLIENT_CPM_KEY_FORMAT,
				clientId.toString()
		);

		return Integer.valueOf(cpmString);
	}

}
