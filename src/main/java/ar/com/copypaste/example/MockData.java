package ar.com.copypaste.example;

import ar.com.copypaste.kafka.entity.ClientPrice;
import ar.com.copypaste.kafka.entity.IncomingEvent;
import ar.com.copypaste.kafka.streams.serialization.JsonEventSerializer;
import org.apache.kafka.clients.producer.*;
import org.apache.kafka.common.serialization.Serdes;

import java.security.SecureRandom;
import java.util.Properties;

public class MockData {

	private final static String INCOMING_EVENT_TOPIC 			= "incoming_events";
	private final static String INCOMING_CLIENT_PRICES_TOPIC	= "client_prices";

	private final static String BOOTSTRAP_SERVERS = "localhost:9092,localhost:9093,localhost:9094";

	private final static SecureRandom random = new SecureRandom();

	private final static String[] eventTypes = {"imp","view","click","lead"};

	private final static Integer MAX_CLIENT_ID = 50;

	public static void main(final String[] args) throws Exception {
		final Mock mock = new Mock();

		mock
			.createClientPricesProducer()
			.createIncomingEventsProducer()
			.generateClientPrices(50)
			.generateIncomingEvents(50000);
	}



	private static Integer randomInt(Integer min, Integer max) {
		return min + random.nextInt((max - min) + 1);
	}


	static class Mock {
		private KafkaProducer<Integer,String> incomingEventsProducer;
		private KafkaProducer<Integer,String> clientPriceProducer;

		private Mock createIncomingEventsProducer() {
			Properties props = new Properties();
			props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, BOOTSTRAP_SERVERS);
			props.put(ProducerConfig.CLIENT_ID_CONFIG, "incoming-events");
			props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, Serdes.Integer().serializer().getClass());
			props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG,JsonEventSerializer.class.getName());

			clientPriceProducer= new KafkaProducer<>(props);
			return this;
		}

		private Mock createClientPricesProducer() {
			Properties props = new Properties();
			props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, BOOTSTRAP_SERVERS);
			props.put(ProducerConfig.CLIENT_ID_CONFIG, "client-prices");
			props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, Serdes.Integer().serializer().getClass());
			props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG,JsonEventSerializer.class.getName());

			incomingEventsProducer = new KafkaProducer<>(props);
			return this;
		}

		private Mock generateClientPrices(Integer count) {
			for(int i=1; i<=count;i++) {
				ClientPrice clientPrice = new ClientPrice(i,randomInt(1,MAX_CLIENT_ID));

				final ProducerRecord clientPriceRecord = new ProducerRecord(
						INCOMING_CLIENT_PRICES_TOPIC,
						clientPrice.getClientID(),
						clientPrice
				);
				clientPriceProducer.send(clientPriceRecord,(recordMetadata,e)->System.out.println(recordMetadata));
			}

			return this;
		}

		private Mock generateIncomingEvents(Integer count) {
			for(int i=1; i<=count;i++) {
				IncomingEvent incomingEvent = new IncomingEvent(
						randomInt(1,MAX_CLIENT_ID),
						eventTypes[randomInt(0,eventTypes.length-1)]
				);

				final ProducerRecord record = new ProducerRecord(INCOMING_EVENT_TOPIC,i,incomingEvent);
				incomingEventsProducer.send(record,(recordMetadata,e)->System.out.println(recordMetadata));
			}
			return this;
		}
	}
}