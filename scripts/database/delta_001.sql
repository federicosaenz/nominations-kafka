CREATE DATABASE IF NOT EXISTS nominations;

use nominations;

DROP TABLE IF EXISTS `nominations`.`cpm`;

CREATE TABLE IF NOT EXISTS `nominations`.`cpm`(
`client_id` int unsigned unique not null,
  `cpm` tinyint unsigned not null,
primary key (`client_id`)
);

DROP TABLE IF EXISTS `nominations`.`clientamount`;

CREATE TABLE IF NOT EXISTS `nominations`.`clientamount`(
  `client_id` int unsigned unique not null,
  `amount` int unsigned not null,
primary key (`client_id`)
);

CREATE USER 'nominations'@'%' IDENTIFIED BY 'ySQS9PtJF5';
GRANT ALL PRIVILEGES ON *.* TO 'nominations'@'%';